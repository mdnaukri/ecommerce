module App.Models {
    export interface POrderModel {
        "prod_img": String;
        "prod_name":string;
        "prod_total_price":number;
        "prod_total_quantity" : number;
        "productId_id":string;
    }
}