var App;
(function (App) {
    var Controllers;
    (function (Controllers) {
        var CheckoutController = /** @class */ (function () {
            function CheckoutController(GetOrder, Orders, TotalQuantityService, $state, POrderService, StorageService, $mdToast, GeoLocationService) {
                this.GetOrder = GetOrder;
                this.Orders = Orders;
                this.TotalQuantityService = TotalQuantityService;
                this.$state = $state;
                this.POrderService = POrderService;
                this.StorageService = StorageService;
                this.$mdToast = $mdToast;
                this.GeoLocationService = GeoLocationService;
                // public name;
                // public email;
                // public phone;
                // public address;
                // public pincode;
                // public city;
                // public state;
                this.finalOrder = {};
                this.orders = Orders;
                this.OrderTotal();
                this.finalOrder = {
                    paymentMethod: "creditcard",
                    orders: this.orders.orders,
                    order_status: 2,
                    _id: this.orders._id
                };
            }
            //public ServerObj = {};
            CheckoutController.prototype.OrderTotal = function () {
                var _this = this;
                this.TotalQuantityService.GetTotalOrderQuantity(this.orders.orders).then(function (ToQuantity) {
                    _this.TQuantity = ToQuantity;
                });
                this.TotalQuantityService.GetTotalOrderPrice(this.orders.orders).then(function (ToPrice) {
                    _this.TPrice = ToPrice;
                });
            };
            CheckoutController.prototype.processForm = function () {
                var _this = this;
                //console.log("Hello we are finally here : " + JSON.stringify(this.finalOrder));
                this.POrderService.placeOrder(this.finalOrder).then(function (response) {
                    _this.$mdToast.show(_this.$mdToast.simple()
                        .textContent('Final Order Successfull! && Checkout')
                        .position('top right')
                        .hideDelay(3000));
                    _this.StorageService.resetStorage();
                    _this.$state.go("home");
                }, function (error) {
                    console.log("Error At Checkout" + error);
                    alert("Error Check With Admin");
                });
            };
            CheckoutController.prototype.getMyLocation = function () {
                var _this = this;
                this.GeoLocationService.getCurrentPosition().then(function (location) {
                    var longitude = JSON.parse(location.longitude);
                    var latitude = JSON.parse(location.latitude);
                    //console.log(longitude , latitude);
                    // var finloc = JSON.parse(location.data.results[0]);
                    // console.log("Hi here is location" + finloc);
                    _this.GeoLocationService.getPhysicalLocation(latitude, longitude).then(function (response) {
                        console.log("In controller" + JSON.stringify(response.data.results[0].formatted_address));
                        if (add = undefined || add == null) {
                            _this.message = false;
                        }
                        var add = response.data.results[0].formatted_address;
                        var res = add.split(",");
                        console.log("Hereee" + res[0]);
                        var pinco = res[res.length - 2].split(" ");
                        _this.finalOrder = {
                            address: add,
                            state: pinco[1],
                            pincode: pinco[2],
                            city: res[res.length - 3]
                        };
                    }, function (error) {
                        console.log("Error at first" + error);
                    });
                }, function (error) {
                    console.log("Error : " + error);
                });
            };
            CheckoutController.$inject = ['GetOrder', 'Orders', 'TotalQuantityService', '$state', 'POrderService', 'StorageService', '$mdToast', 'GeoLocationService'];
            return CheckoutController;
        }());
        Controllers.CheckoutController = CheckoutController;
    })(Controllers = App.Controllers || (App.Controllers = {}));
})(App || (App = {}));
app.controller('CheckoutController', App.Controllers.CheckoutController);

//# sourceMappingURL=../source-maps/Controllers/CheckoutController.js.map
