module App.Controllers {

    import POrder = App.Models.POrderModel;
    interface IHomeControllerScope extends ng.IScope {
        homeCntrl: HomeController;
    }

    interface IHomeControllerBindings {

    }

    interface IHomeController extends IHomeControllerBindings {

    }
    export class HomeController implements IHomeController {
        public prodList;
        Buy(productName, productPrice, productId, prod_img) {

            this.$mdDialog.show({
                controller: AddOrRemoveProductsController,
                controllerAs: 'arCntrl',
                templateUrl: 'app/templates/addremove.html',
                locals: { productName: productName, productPrice: productPrice, productId: productId, prod_img: prod_img, state: 0 },
                clickOutsideToClose: true
            }).then(() => {
                //console.log("yupp md");

            });

        }

        AddToCart(productName, productPrice, productId, prod_img) {
            this.$mdDialog.show({
                controller: AddOrRemoveProductsController,
                controllerAs: 'arCntrl',
                templateUrl: 'app/templates/addremove.html',
                locals: { productName: productName, productPrice: productPrice, productId: productId, prod_img: prod_img, state: 1 },
                clickOutsideToClose: true
            }).then(() => {
                //console.log("yupp md");

            });

        }
        static $inject = ['ProductListService', 'AllProducts', '$mdDialog']
        constructor(private ProductListService: App.Services.ProductListService, private AllProducts, private readonly $mdDialog: ng.material.IDialogService) {
            this.prodList = AllProducts;
        }
    }


    //Add REmove Class starts here
    export class AddOrRemoveProductsController {
        public quantity: number;
        public prod_total_price: number;
        cancel() {
            //close md dialog 
            this.$mdDialog.hide();
        }


        save() {

            this.prod_total_price = this.quantity * this.productPrice;
            const OrderObj: POrder = {
                prod_img: this.prod_img,
                prod_name: this.productName,
                prod_total_price: this.prod_total_price,
                productId_id: this.productId,
                prod_total_quantity: this.quantity
            }

        this.StorageService.checkInStorage(OrderObj).then((a)=>{
            //console.log("GIIIGI " +JSON.stringify(a ));
            this.POrderService.placeOrder(a).then((response) => {
                //console.log("Hello res : " + response);
                this.StorageService.saveInStorage(response);
                this.$mdDialog.hide();
                if (this.state == 0) {
                    this.$state.go("checkout.personaldt");
                } else {
                    this.$state.go("cart");
                }
                this.$mdToast.show(
                    this.$mdToast.simple()
                      .textContent('Order Successfull!')
                      .position('top right')
                      .hideDelay(4000));
            });
        });


            // else {
            //     this.POrderService.placeOrder(a).then((response) => {
            //         console.log("Hello res : " + response);
            //         this.StorageService.saveInStorage(response);
            //         this.$mdDialog.hide();
            //         if (this.state == 0) {
            //             this.$state.go("checkout.personaldt");
            //         } else {
            //             this.$state.go("cart");
            //         }
            //     });
            // }

        }
        decreaseQuantity() {
            this.quantity = this.quantity - 1;
            if (this.quantity == -1) {
                this.quantity = 1;
            }
        }

        increaseQuantity() {

            this.quantity = this.quantity + 1;

            //console.log("hi we are increase");
        }
        static $inject = ['$mdDialog', 'POrderService', 'productName', 'productPrice', '$state', 'productId', 'prod_img', 'state', 'StorageService','$mdToast'];
        constructor(private readonly $mdDialog: ng.material.IDialogService, private POrderService: App.Services.POrderService,
            private productName, private productPrice, private $state, private productId, private prod_img, private state, private StorageService: App.Services.StorageService , private $mdToast) {
            this.quantity = 1;
        }
    }
    //Add Remove ends here

}




app.controller('HomeController', App.Controllers.HomeController);
app.controller('AddOrRemoveProductsController', App.Controllers.AddOrRemoveProductsController);