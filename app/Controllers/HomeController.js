var App;
(function (App) {
    var Controllers;
    (function (Controllers) {
        var HomeController = /** @class */ (function () {
            function HomeController(ProductListService, AllProducts, $mdDialog) {
                this.ProductListService = ProductListService;
                this.AllProducts = AllProducts;
                this.$mdDialog = $mdDialog;
                this.prodList = AllProducts;
            }
            HomeController.prototype.Buy = function (productName, productPrice, productId, prod_img) {
                this.$mdDialog.show({
                    controller: AddOrRemoveProductsController,
                    controllerAs: 'arCntrl',
                    templateUrl: 'app/templates/addremove.html',
                    locals: { productName: productName, productPrice: productPrice, productId: productId, prod_img: prod_img, state: 0 },
                    clickOutsideToClose: true
                }).then(function () {
                    //console.log("yupp md");
                });
            };
            HomeController.prototype.AddToCart = function (productName, productPrice, productId, prod_img) {
                this.$mdDialog.show({
                    controller: AddOrRemoveProductsController,
                    controllerAs: 'arCntrl',
                    templateUrl: 'app/templates/addremove.html',
                    locals: { productName: productName, productPrice: productPrice, productId: productId, prod_img: prod_img, state: 1 },
                    clickOutsideToClose: true
                }).then(function () {
                    //console.log("yupp md");
                });
            };
            HomeController.$inject = ['ProductListService', 'AllProducts', '$mdDialog'];
            return HomeController;
        }());
        Controllers.HomeController = HomeController;
        //Add REmove Class starts here
        var AddOrRemoveProductsController = /** @class */ (function () {
            function AddOrRemoveProductsController($mdDialog, POrderService, productName, productPrice, $state, productId, prod_img, state, StorageService, $mdToast) {
                this.$mdDialog = $mdDialog;
                this.POrderService = POrderService;
                this.productName = productName;
                this.productPrice = productPrice;
                this.$state = $state;
                this.productId = productId;
                this.prod_img = prod_img;
                this.state = state;
                this.StorageService = StorageService;
                this.$mdToast = $mdToast;
                this.quantity = 1;
            }
            AddOrRemoveProductsController.prototype.cancel = function () {
                //close md dialog 
                this.$mdDialog.hide();
            };
            AddOrRemoveProductsController.prototype.save = function () {
                var _this = this;
                this.prod_total_price = this.quantity * this.productPrice;
                var OrderObj = {
                    prod_img: this.prod_img,
                    prod_name: this.productName,
                    prod_total_price: this.prod_total_price,
                    productId_id: this.productId,
                    prod_total_quantity: this.quantity
                };
                this.StorageService.checkInStorage(OrderObj).then(function (a) {
                    //console.log("GIIIGI " +JSON.stringify(a ));
                    _this.POrderService.placeOrder(a).then(function (response) {
                        //console.log("Hello res : " + response);
                        _this.StorageService.saveInStorage(response);
                        _this.$mdDialog.hide();
                        if (_this.state == 0) {
                            _this.$state.go("checkout.personaldt");
                        }
                        else {
                            _this.$state.go("cart");
                        }
                        _this.$mdToast.show(_this.$mdToast.simple()
                            .textContent('Order Successfull!')
                            .position('top right')
                            .hideDelay(4000));
                    });
                });
                // else {
                //     this.POrderService.placeOrder(a).then((response) => {
                //         console.log("Hello res : " + response);
                //         this.StorageService.saveInStorage(response);
                //         this.$mdDialog.hide();
                //         if (this.state == 0) {
                //             this.$state.go("checkout.personaldt");
                //         } else {
                //             this.$state.go("cart");
                //         }
                //     });
                // }
            };
            AddOrRemoveProductsController.prototype.decreaseQuantity = function () {
                this.quantity = this.quantity - 1;
                if (this.quantity == -1) {
                    this.quantity = 1;
                }
            };
            AddOrRemoveProductsController.prototype.increaseQuantity = function () {
                this.quantity = this.quantity + 1;
                //console.log("hi we are increase");
            };
            AddOrRemoveProductsController.$inject = ['$mdDialog', 'POrderService', 'productName', 'productPrice', '$state', 'productId', 'prod_img', 'state', 'StorageService', '$mdToast'];
            return AddOrRemoveProductsController;
        }());
        Controllers.AddOrRemoveProductsController = AddOrRemoveProductsController;
        //Add Remove ends here
    })(Controllers = App.Controllers || (App.Controllers = {}));
})(App || (App = {}));
app.controller('HomeController', App.Controllers.HomeController);
app.controller('AddOrRemoveProductsController', App.Controllers.AddOrRemoveProductsController);

//# sourceMappingURL=../source-maps/Controllers/HomeController.js.map
