
module App.Controllers {
    interface ICartControllerScope extends ng.IScope {
        cartCntrl: CartController;
    }

    interface ICartControllerBindings {
        orders : any;
        TQuantity: any;
        TPrice:any;
    }

    interface ICartController extends ICartControllerBindings{
       
    }

    export class CartController implements ICartController{
        public TQuantity: any;
        public TPrice:any;
        public orders: any;

        OrderTotal(){
            this.TotalQuantityService.GetTotalOrderQuantity(this.orders.orders).then((ToQuantity)=>{
                this.TQuantity = ToQuantity;
        });

        this.TotalQuantityService.GetTotalOrderPrice(this.orders.orders).then((ToPrice)=>{
                this.TPrice = ToPrice;
        });

    }
        RedirectToCheckout(){
            this.$state.go("checkout.personaldt");
        }
        static $inject = ['$http','GetOrder','Orders','$state','TotalQuantityService'];
        constructor(private $http, private GetOrder, private Orders, private $state, private TotalQuantityService:App.Services.TotalQuantityService){
                this.orders = Orders;
                //console.log("Here is our data 1" + JSON.stringify(this.orders));
                this.OrderTotal();
        }
        
    }  
      
}
app.controller('CartController',App.Controllers.CartController);