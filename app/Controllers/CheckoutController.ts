module App.Controllers {
    interface ICheckoutControllerScope extends ng.IScope {
        chOCntrl: App.Controllers.CheckoutController;
    }

    interface ICheckoutControllerBindings {

    }

    interface ICheckoutController extends ICheckoutControllerBindings {

    }

    export class CheckoutController implements ICheckoutController {
        public orders;
        public TQuantity: any;
        public TPrice: any;
        public paymentMethod;
        public message;
        // public name;
        // public email;
        // public phone;
        // public address;
        // public pincode;
        // public city;
        // public state;
        public finalOrder = {};
        //public ServerObj = {};
        OrderTotal() {

            this.TotalQuantityService.GetTotalOrderQuantity(this.orders.orders).then((ToQuantity) => {
                this.TQuantity = ToQuantity;
            });

            this.TotalQuantityService.GetTotalOrderPrice(this.orders.orders).then((ToPrice) => {
                this.TPrice = ToPrice;
            });
        }



        processForm() { 
           
            //console.log("Hello we are finally here : " + JSON.stringify(this.finalOrder));
            this.POrderService.placeOrder(this.finalOrder).then((response)=>{
                this.$mdToast.show(
                    this.$mdToast.simple()
                      .textContent('Final Order Successfull! && Checkout')
                      .position('top right')
                      .hideDelay(3000));
                this.StorageService.resetStorage();
                this.$state.go("home");
                
            },(error)=>{
                    console.log("Error At Checkout" + error);
                    alert("Error Check With Admin");
            });
            
        }


        getMyLocation(){
            this.GeoLocationService.getCurrentPosition().then((location)=>{
            var longitude  = JSON.parse(location.longitude);
            var latitude = JSON.parse(location.latitude);
            //console.log(longitude , latitude);
            // var finloc = JSON.parse(location.data.results[0]);
            // console.log("Hi here is location" + finloc);
            
            this.GeoLocationService.getPhysicalLocation(latitude ,longitude).then((response)=>{
                console.log("In controller" + JSON.stringify(response.data.results[0].formatted_address));
                if(add = undefined || add== null){
                    this.message = false;
                }
                var add = response.data.results[0].formatted_address;
                
                var res = add.split(",");
                console.log("Hereee" + res[0]);
                var pinco = res[res.length-2].split(" ");
                this.finalOrder = {
                    address: add,
                    state: pinco[1],
                    pincode :pinco[2],
                    city: res[res.length-3]
                    }
                
            },(error)=>{
                    console.log("Error at first" + error)
            })
            },(error)=>{
                    console.log("Error : " + error);
            });
        }

        static $inject = ['GetOrder', 'Orders', 'TotalQuantityService','$state','POrderService','StorageService','$mdToast','GeoLocationService']
        constructor(private GetOrder, private Orders, private TotalQuantityService: App.Services.TotalQuantityService,private $state, private POrderService:App.Services.POrderService , private StorageService: App.Services.StorageService, private $mdToast, private GeoLocationService: App.Services.GeoLocationService) {
            this.orders = Orders;
            this.OrderTotal();
            this.finalOrder = { 
            paymentMethod: "creditcard" ,
            orders : this.orders.orders,
            order_status: 2,
            _id : this.orders._id
            }

        }
    }
}

app.controller('CheckoutController', App.Controllers.CheckoutController);