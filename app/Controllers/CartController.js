var App;
(function (App) {
    var Controllers;
    (function (Controllers) {
        var CartController = /** @class */ (function () {
            function CartController($http, GetOrder, Orders, $state, TotalQuantityService) {
                this.$http = $http;
                this.GetOrder = GetOrder;
                this.Orders = Orders;
                this.$state = $state;
                this.TotalQuantityService = TotalQuantityService;
                this.orders = Orders;
                //console.log("Here is our data 1" + JSON.stringify(this.orders));
                this.OrderTotal();
            }
            CartController.prototype.OrderTotal = function () {
                var _this = this;
                this.TotalQuantityService.GetTotalOrderQuantity(this.orders.orders).then(function (ToQuantity) {
                    _this.TQuantity = ToQuantity;
                });
                this.TotalQuantityService.GetTotalOrderPrice(this.orders.orders).then(function (ToPrice) {
                    _this.TPrice = ToPrice;
                });
            };
            CartController.prototype.RedirectToCheckout = function () {
                this.$state.go("checkout.personaldt");
            };
            CartController.$inject = ['$http', 'GetOrder', 'Orders', '$state', 'TotalQuantityService'];
            return CartController;
        }());
        Controllers.CartController = CartController;
    })(Controllers = App.Controllers || (App.Controllers = {}));
})(App || (App = {}));
app.controller('CartController', App.Controllers.CartController);

//# sourceMappingURL=../source-maps/Controllers/CartController.js.map
