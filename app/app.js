///<reference path="_all.d.ts"/>
var app = angular.module('app', ['ui.router', 'ngAnimate', 'ngStorage', 'ngMaterial', 'ngMdIcons']);
function routeConfig($stateProvider, $urlRouterProvider, $locationProvider, $mdIconProvider, $mdThemingProvider) {
    //location provider is only used to make urls seo-friendly or hash free
    $locationProvider.html5Mode(true);
    //state provider is used to render the ui as its a function of ui-router
    $stateProvider
        .state('home', {
        url: '/home',
        views: {
            header: {
                templateUrl: 'app/templates/header.html'
            },
            main: {
                templateUrl: 'app/templates/home.html',
                controller: 'HomeController',
                controllerAs: 'homeCntrl'
            }
        },
        resolve: {
            AllProducts: function (ProductListService) {
                return ProductListService.getAllProducts;
            }
        }
    })
        .state('cart', {
        url: '/cart',
        views: {
            header: {
                templateUrl: 'app/templates/header.html'
            },
            main: {
                templateUrl: 'app/templates/cart.html',
                controller: 'CartController',
                controllerAs: 'cartCntrl'
            }
        },
        resolve: {
            order_ID: function (StorageService) {
                return StorageService.GetOrderIdFromStorage();
            },
            Orders: function (GetOrder, order_ID) {
                return GetOrder.getAllOrderById(order_ID);
            }
        }
    })
        .state('checkout', {
        url: '/checkout',
        views: {
            header: {
                templateUrl: 'app/templates/header.html'
            },
            main: {
                templateUrl: 'app/templates/checkout.html',
                controller: 'CheckoutController',
                controllerAs: 'chOCntrl'
            }
        }, resolve: {
            order_ID: function (StorageService) {
                return StorageService.GetOrderIdFromStorage();
            },
            Orders: function (GetOrder, order_ID) {
                return GetOrder.getAllOrderById(order_ID);
            }
        }
    })
        .state('checkout.personaldt', {
        url: '/personal',
        templateUrl: 'app/templates/personal.html'
    })
        .state('checkout.deliverydt', {
        url: '/delivery',
        templateUrl: 'app/templates/delivery.html'
    })
        .state('checkout.ordersmry', {
        url: '/ordersummary',
        templateUrl: 'app/templates/ordersummary.html'
    })
        .state('checkout.payopt', {
        url: '/paymentoptions',
        templateUrl: 'app/templates/paymentoptions.html'
    });
    $urlRouterProvider.otherwise('/home');
}
app.config(['$stateProvider', '$urlRouterProvider', '$locationProvider', routeConfig]);

//# sourceMappingURL=source-maps/app.js.map
