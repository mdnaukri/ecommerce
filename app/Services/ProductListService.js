var App;
(function (App) {
    var Services;
    (function (Services) {
        var ProductListService = /** @class */ (function () {
            function ProductListService($http) {
                this.$http = $http;
                this.getAllProducts = $http.get('http://localhost:4000/api/getAllProduct').then(onSuccess, onError);
                function onSuccess(response) {
                    //console.log("WE r here : " + JSON.stringify(response.data));
                    return response.data;
                }
                ;
                function onError(response) {
                    alert("Error");
                }
                ;
            }
            ProductListService.$inject = ['$http'];
            return ProductListService;
        }());
        Services.ProductListService = ProductListService;
    })(Services = App.Services || (App.Services = {}));
})(App || (App = {}));
app.service('ProductListService', App.Services.ProductListService);

//# sourceMappingURL=../source-maps/Services/ProductListService.js.map
