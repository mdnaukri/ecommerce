var App;
(function (App) {
    var Services;
    (function (Services) {
        var TotalQuantityService = /** @class */ (function () {
            function TotalQuantityService($q) {
                this.$q = $q;
            }
            TotalQuantityService.prototype.GetTotalOrderQuantity = function (orders) {
                var promise = this.$q.defer();
                var totalq = orders.reduce(function (preVal, order) { return preVal + order.prod_total_quantity; }, 0);
                promise.resolve(totalq);
                return promise.promise;
            };
            TotalQuantityService.prototype.GetTotalOrderPrice = function (orders) {
                var promise1 = this.$q.defer();
                var totalp = orders.reduce(function (preVal, order) { return preVal + order.prod_total_price; }, 0);
                promise1.resolve(totalp);
                return promise1.promise;
            };
            TotalQuantityService.$inject = ['$q'];
            return TotalQuantityService;
        }());
        Services.TotalQuantityService = TotalQuantityService;
    })(Services = App.Services || (App.Services = {}));
})(App || (App = {}));
app.service('TotalQuantityService', App.Services.TotalQuantityService);

//# sourceMappingURL=../source-maps/Services/TotalQuantityService.js.map
