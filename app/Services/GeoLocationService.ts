module App.Services {
    export class GeoLocationService{
        getCurrentPosition() {
            var deferred = this.$q.defer();
    
            if (!this.$window.navigator.geolocation) {
                deferred.reject('Geolocation not supported.');
            } else {
                this.$window.navigator.geolocation.getCurrentPosition((position)=> {
                    deferred.resolve(position.coords);
                    },(err)=> {
                        deferred.reject(err);
                    });
            }
    
            return deferred.promise;
        }

        getPhysicalLocation(lat,long){
            return  this.$http.get("https://maps.googleapis.com/maps/api/geocode/json?latlng="+lat+ ","+long).then((address)=>{
                //console.log("I am from service " + JSON.stringify(address));
                return address;
                    },(error)=>{console.log(error); });
        }
      

        static $inject = ['$q', '$window','$http']
        constructor(private $q, private $window, private $http){

        }
    }
}

app.service('GeoLocationService', App.Services.GeoLocationService);