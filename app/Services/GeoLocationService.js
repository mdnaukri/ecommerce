var App;
(function (App) {
    var Services;
    (function (Services) {
        var GeoLocationService = /** @class */ (function () {
            function GeoLocationService($q, $window, $http) {
                this.$q = $q;
                this.$window = $window;
                this.$http = $http;
            }
            GeoLocationService.prototype.getCurrentPosition = function () {
                var deferred = this.$q.defer();
                if (!this.$window.navigator.geolocation) {
                    deferred.reject('Geolocation not supported.');
                }
                else {
                    this.$window.navigator.geolocation.getCurrentPosition(function (position) {
                        deferred.resolve(position.coords);
                    }, function (err) {
                        deferred.reject(err);
                    });
                }
                return deferred.promise;
            };
            GeoLocationService.prototype.getPhysicalLocation = function (lat, long) {
                return this.$http.get("https://maps.googleapis.com/maps/api/geocode/json?latlng=" + lat + "," + long).then(function (address) {
                    //console.log("I am from service " + JSON.stringify(address));
                    return address;
                }, function (error) { console.log(error); });
            };
            GeoLocationService.$inject = ['$q', '$window', '$http'];
            return GeoLocationService;
        }());
        Services.GeoLocationService = GeoLocationService;
    })(Services = App.Services || (App.Services = {}));
})(App || (App = {}));
app.service('GeoLocationService', App.Services.GeoLocationService);

//# sourceMappingURL=../source-maps/Services/GeoLocationService.js.map
