module App.Services {

    export class TotalQuantityService {

        GetTotalOrderQuantity(orders) {
            var promise = this.$q.defer();

            var totalq = orders.reduce((preVal, order) => preVal + order.prod_total_quantity, 0);
            promise.resolve(totalq);
            return promise.promise;
        }

        GetTotalOrderPrice(orders) {
            var promise1 = this.$q.defer();

            var totalp = orders.reduce((preVal, order) => preVal + order.prod_total_price, 0);
            promise1.resolve(totalp);
            return promise1.promise;
        }
        static $inject = ['$q']
        constructor(private $q) { }

    }

}

app.service('TotalQuantityService', App.Services.TotalQuantityService)