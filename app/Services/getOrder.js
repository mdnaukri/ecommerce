var App;
(function (App) {
    var Services;
    (function (Services) {
        var GetOrder = /** @class */ (function () {
            function GetOrder($http, StorageService) {
                this.$http = $http;
                this.StorageService = StorageService;
            }
            //api/allorders
            GetOrder.prototype.getAllOrderById = function (resp) {
                return this.$http.get('http://localhost:4000/api/orderById/' + resp).then(function (response) {
                    //console.log("Im here" + JSON.stringify(response.data));
                    return response.data;
                }, function (error) {
                    console.log("Erro At Ger" + error);
                });
            };
            GetOrder.$inject = ['$http', 'StorageService'];
            return GetOrder;
        }());
        Services.GetOrder = GetOrder;
    })(Services = App.Services || (App.Services = {}));
})(App || (App = {}));
app.service('GetOrder', App.Services.GetOrder);

//# sourceMappingURL=../source-maps/Services/getOrder.js.map
