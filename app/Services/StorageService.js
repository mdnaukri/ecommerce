var App;
(function (App) {
    var Services;
    (function (Services) {
        var StorageService = /** @class */ (function () {
            function StorageService($localStorage, $q) {
                this.$localStorage = $localStorage;
                this.$q = $q;
            }
            //public order;
            StorageService.prototype.saveInStorage = function (data) {
                this.$localStorage.$reset();
                this.$localStorage["orders"] = { order: data };
                this.$localStorage.$apply();
                this.$localStorage.$sync();
            };
            StorageService.prototype.resetStorage = function () {
                this.$localStorage.$reset();
            };
            StorageService.prototype.checkInStorage = function (data) {
                var myDeferred = this.$q.defer();
                var ordersFromStorage = this.$localStorage["orders"];
                //console.log("Hie we are in : " + ordersFromStorage);
                if (ordersFromStorage == undefined) {
                    var findata = { orders: data, order_status: 1 };
                    //console.log("First : " + JSON.stringify(data));
                    myDeferred.resolve(findata);
                }
                else {
                    //ordersFromStorage.order.orders.forEach(orstorage => {
                    for (var i = 0; i < ordersFromStorage.order.orders.length; i++) {
                        if (ordersFromStorage.order.orders[i].productId_id == data.productId_id) {
                            //console.log("Here is out element : " + JSON.stringify(element));
                            //console.log("Here is out our data : "+ JSON.stringify(data));
                            //var index = ordersFromStorage.order.orders.indexOf(i);
                            //console.log("WEll " +JSON.stringify(data) );
                            ordersFromStorage.order.orders[i] = data;
                            //console.log("Second : " + JSON.stringify(ordersFromStorage));
                        }
                        else {
                            ordersFromStorage.order.orders.push(data);
                            //console.log("Third : " + JSON.stringify(ordersFromStorage.order));
                        }
                        break;
                    }
                    //);
                    myDeferred.resolve(ordersFromStorage.order);
                }
                return myDeferred.promise;
            };
            StorageService.prototype.GetOrderIdFromStorage = function () {
                var orderIdDefer = this.$q.defer();
                var ordersFromStorage = this.$localStorage["orders"];
                //console.log("Hie we are in : " + ordersFromStorage);
                if (ordersFromStorage == undefined) {
                    orderIdDefer.reject();
                }
                else {
                    orderIdDefer.resolve(ordersFromStorage.order._id);
                }
                return orderIdDefer.promise;
            };
            StorageService.$inject = ['$localStorage', '$q'];
            return StorageService;
        }());
        Services.StorageService = StorageService;
    })(Services = App.Services || (App.Services = {}));
})(App || (App = {}));
app.service('StorageService', App.Services.StorageService);

//# sourceMappingURL=../source-maps/Services/StorageService.js.map
