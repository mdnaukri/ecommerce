var App;
(function (App) {
    var Services;
    (function (Services) {
        var POrderService = /** @class */ (function () {
            function POrderService($http) {
                this.$http = $http;
            }
            POrderService.prototype.getAllProducts = function () {
                return this.$http.get('http://localhost:4000/app/data/data.json').then(function (response) {
                    //console.log(response.data);
                    return response.data;
                }), (function (response) {
                    alert("Error");
                });
            };
            POrderService.prototype.placeOrder = function (order) {
                return this.$http.post('http://localhost:4000/api/orders', { order: order }).then(function (response) {
                    //console.log("Hi here i have sm : "+ JSON.stringify(response.data));
                    return response.data;
                }, function (error) { console.log("Error exist" + error); });
            };
            POrderService.$inject = ['$http'];
            return POrderService;
        }());
        Services.POrderService = POrderService;
    })(Services = App.Services || (App.Services = {}));
})(App || (App = {}));
app.service('POrderService', App.Services.POrderService);

//# sourceMappingURL=../source-maps/Services/POrderService.js.map
