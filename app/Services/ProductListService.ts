module App.Services{
    
    export class ProductListService {
          
        public getAllProducts;
        static $inject = ['$http'];
        constructor(private $http:ng.IHttpService){
            this.getAllProducts = $http.get('http://localhost:4000/api/getAllProduct').then(onSuccess, onError);
            function onSuccess(response): any {
                //console.log("WE r here : " + JSON.stringify(response.data));
                return response.data;
            };

            function onError(response) {
                alert("Error");
            };
        }
    }
}

app.service('ProductListService',App.Services.ProductListService)