var express = require('express'),
    path = require('path'),
    rootPath = path.normalize(__dirname + '/../'),
    mongoose = require('mongoose'),
    apiRouter = express.Router(),
    expressJwt = require('express-jwt');

module.exports = function (app) {
    app.use(express.json());
    app.use(express.urlencoded());
    app.use('/api', apiRouter);
    require('./api/allRoutes')(apiRouter);
    
  app.get('/*', function (req, res) {
        res.sendFile(rootPath + '/index.html')
    });

    app.use(function (req, res) {
        res.status(404);
        res.render('404');
        return;
    })

}