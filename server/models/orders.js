var mongoose = require('mongoose');

var OrdersSchema = new mongoose.Schema({
        "name" :{type:String},
        "phone": {type: String},
        "email": {type: String},
        "address": {type: String},
        "pincode": {type: String},
        "city": {type: String},
        "state": {type: String},
        "orders": [],
        "order_status":  {type: Number},
    
});

module.exports = mongoose.model('Orders', OrdersSchema)