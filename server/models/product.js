const mongoose = require('mongoose');

var ProductSchema = new mongoose.Schema({
    "prod_img":{type:String , required: '{PATH} is required!'},
    "prod_name":{type:String , required: '{PATH} is required!'},
    "prod_price":{type:Number , required: '{PATH} is required!'}
});

module.exports = mongoose.model('product',ProductSchema);