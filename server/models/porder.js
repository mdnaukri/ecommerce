const mongoose = require('mongoose');

var POrderSchema = new mongoose.Schema({
    "prod_img":{type:String , required: '{PATH} is required!'},
    "prod_name":{type:String , required: '{PATH} is required!'},
    "prod_total_price":{type:Number , required: '{PATH} is required!'},
    "prod_total_quantity":{type:Number , required: '{PATH} is required!'}
});

module.exports = mongoose.model('POrder',POrderSchema);