    module.exports = function(apiRouter, mong){
    require('./addproductfromapi')(apiRouter);
    require('./getAllProduct')(apiRouter);
    require('./orders')(apiRouter);
}